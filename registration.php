<?php
/**
 * Copyright © Hatimeria. All rights reserved.
 * See LICENSE.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Hatimeria_FailedLoginLogs',
    __DIR__
);