<?php
/**
 * Copyright © Hatimeria. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Hatimeria\FailedLoginLogs\Observer;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Hatimeria\FailedLoginLogs\Logger\Logger;

class FailedLogin implements ObserverInterface
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var RemoteAddress
     */
    private $remoteAddress;

    /**
     * Failed Login constructor.
     *
     * @param Logger $logger
     * @param RemoteAddress $remoteAddress
     */
    public function __construct(Logger $logger, RemoteAddress $remoteAddress)
    {
        $this->logger = $logger;
        $this->remoteAddress = $remoteAddress;
    }

    /**
     * Execute method.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $longIp = $this->remoteAddress->getRemoteAddress();
        $this->logger->info($longIp);
    }
}