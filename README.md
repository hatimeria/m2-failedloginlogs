Hatimeria/Failed Login Logs Magento2 module
==============================

This module logs failed backend login attempts in /var/log/loginlogs_admin.log

## Installation

```
    ...
    "repositories": [
        ... ,
        {
            "type": "git",
            "url": "git@bitbucket.org:hatimeria/m2-failedloginlogs.git"
        }
    ],
    "require": {
        ... ,
        "hatimeria/m2-failedloginlogs": "dev-master"
    },
    ...
```