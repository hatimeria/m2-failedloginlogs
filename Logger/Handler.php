<?php
/**
 * Copyright © Hatimeria. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Hatimeria\FailedLoginLogs\Logger;

use Monolog\Logger;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Logger\Handler\Base;
use Monolog\Formatter\LineFormatter;

class Handler extends Base
{
    /**
     * Log format
     */
    const FORMAT = "[%datetime%] %message%\n";

    /**
     * Logging level
     *
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * Log file name
     *
     * @var string
     */
    protected $fileName = 'loginlogs_admin.log';

    /**
     * Handler constructor
     *
     * @param DriverInterface $filesystem
     * @param DirectoryList $directoryList
     */
    public function __construct(
        DriverInterface $filesystem,
        DirectoryList $directoryList
    ) {
        $this->directoryList = $directoryList;
        $filePath = $this->directoryList->getPath('log') . DIRECTORY_SEPARATOR;
        parent::__construct(
            $filesystem,
            $filePath
        );
        $this->setFormatter(new LineFormatter(self::FORMAT, null, true));
    }
}